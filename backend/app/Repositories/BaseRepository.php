<?php
namespace App\Repositories;

abstract Class BaseRepository
{
    abstract protected function getModel();

    protected $_model;

    public function __construct() {
        $this->_load_model($this->getModel());
    }

    public function _load_model($model) {
        $this->_model = app($model);
        if (!is_subclass_of($this->_model, \Illuminate\Database\Eloquent\Model::class)) {
            throw new \Exception("Invalid Model.");
        }
    }

	public function __call($method, array $parameters)
	{
		// dd($this->_model);
		return call_user_func_array([$this->_model, $method], $parameters);
	}

}