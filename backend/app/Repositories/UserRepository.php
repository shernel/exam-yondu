<?php
namespace App\Repositories;

Class UserRepository extends BaseRepository implements Interfaces\UserRepositoryInterface
{

    protected function getModel() {
        return Models\User::class;
    }

    public function is_username_exists(String $username, Array $exludeids = []) {
        $model = $this->where('username', $username);
        if(!empty($exludeids)){
            $model->whereNotIn('id', $exludeids);
        }
        return $model->exists();
    }

    public function is_email_exists(String $email, Array $exludeids = []) {
        $model = $this->where('email', $email);
        if(!empty($exludeids)){
            $model->whereNotIn('id', $exludeids);
        }
        return $model->exists();

    }

}