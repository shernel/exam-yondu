<?php
namespace App\Repositories\Interfaces;

interface UserRepositoryInterface
{
    public function _load_model($model);

    public function is_username_exists(String $username, Array $exludeids = []);
    public function is_email_exists(String $email, Array $exludeids = []);
}