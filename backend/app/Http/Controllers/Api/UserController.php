<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validations\Validator;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $_user;
    protected $_validator;

    public function __construct(Request $request, UserRepository $user, Validator $validator)
    {
        $this->_validator = $validator;
        $this->_user = $user;

    }
    public function index()
    {
        return $this->_user->select(
            'id', 'username', 'firstname', 'lastname', 'email', 'contact', 'address', 'postcode'
        )->get();
    }
    public function store(Request $request)
    {
        if (! $this->_validator->loadRule(new \App\Http\Validations\Rules\UserStoreRules)->hasError()
        && $this->_user->create( array_filter($request->all())) )
        {
            return response()->json(['success' => 1]);
        }

        return response()->json($this->_validator->errors(), 400);
    }
    public function show($id)
    {
        return $this->_user->where('id', $id)->select(
            'id', 'username', 'firstname', 'lastname', 'email', 'contact', 'address', 'postcode'
        )->first();
    }
    public function update($id, Request $request)
    {
        if (! $this->_validator->loadRule(new \App\Http\Validations\Rules\UserUpdateRules)->hasError()
        && $this->_user->find($id)->update( array_filter($request->all())) )
        {
            return response()->json(['success' => 1]);
        }
        return response()->json($this->_validator->errors(), 400);
    }

    public function destroy($id)
    {
        if ($this->_user->find($id)->delete())
        {
            return response()->json(['success' => 1]);
        }
    }

    // Batch Delete
    public function destroys(Request $request)
    {
        if ($this->_user->whereIn('id', $request->ids)->delete())
        {
            return response()->json(['success' => 1]);
        }
    }
}