<?php

namespace App\Http\Validations\Rules;

use Illuminate\Validation\Validator;
use App\Repositories\UserRepository;

class UserStoreRules implements \App\Http\Validations\Interfaces\ValidatorRulesInterface
{
    public function validate(Array $values): Validator {
        $rules =  [
            'username' => 'required|unique:ref_users|max:255',
            'password' => 'required|same:confirm_password|max:255',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'email|unique:ref_users|max:255',
        ];

        if( !trim(request()->email) ) {
            unset($rules['email']); // make email optional
        }

        return \Validator::make($values, $rules)
                ->after(function ($validator) {

                });

    }
}
