<?php

namespace App\Http\Validations\Rules;

use Illuminate\Validation\Validator;
use App\Repositories\UserRepository;

class UserUpdateRules implements \App\Http\Validations\Interfaces\ValidatorRulesInterface
{
    public function validate(Array $values): Validator {
        $rules =  [
            'username' => 'required|max:255',
            'password' => 'same:confirm_password|max:255',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'email|max:255',
        ];
        if( !trim(request()->email) ) {
            unset($rules['email']); // make email optional
        }

        return \Validator::make($values, $rules)
                ->after(function ($validator) {
                    if ( request()->username && app(UserRepository::class)
                            ->is_username_exists(request()->username, [request()->segment(3)]) )
                    {
                        $validator->errors()->add('username', 'The email has already been taken.');
                    }
                    if ( request()->email && app(UserRepository::class)
                            ->is_email_exists(request()->email, [request()->segment(3)]) )
                    {
                        $validator->errors()->add('email', 'The email has already been taken.');
                    }
                });

    }
}
