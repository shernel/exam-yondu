<?php

namespace App\Http\Validations;

use Illuminate\Http\Request;

class Validator implements Interfaces\ValidatorInterface
{
    protected $_validator;

    public function loadRule(Interfaces\ValidatorRulesInterface $rules) {
        $this->_validator = $rules->validate(request()->all());

        return $this;
    }

    public function hasError() {
        return $this->_validator->fails();
    }

    public function errors() {
        return $this->_validator->errors();
    }

}
