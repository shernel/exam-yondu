<?php
namespace App\Http\Validations\Interfaces;

interface ValidatorInterface
{
    public function loadRule(ValidatorRulesInterface $rules);

    public function hasError();

    public function errors();
}