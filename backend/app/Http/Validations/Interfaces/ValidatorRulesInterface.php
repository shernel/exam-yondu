<?php
namespace App\Http\Validations\Interfaces;

use Illuminate\Validation\Validator;

interface ValidatorRulesInterface
{
    public function validate(Array $values): Validator;
}