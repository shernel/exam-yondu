<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        app(\App\Repositories\UserRepository::class)->create([
            'username'  => 'admin',
            'password'  => 'admin',
            'firstname'  => 'Administrator',
            'lastname'  => '',
            'email'  => '',
            'contact'  => '',
            'address'  => '',
            'postcode'  => ''
        ]);

        DB::table('oauth_clients')->insert([
            'id' => 1,
            'name' => 'frontend-spa',
            'secret' => 'qJI5J2wCfnIsJ7oU9xZ5shqkmk2nrlKbIH0Jnknx',
            'redirect' => '/',
            'personal_access_client' =>0,
            'password_client' => 1,
            'revoked' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

    }
}
