<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use App\Repositories\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCanRequestToUserIndexAndValidateToken()
    {
        $response = $this->json('GET', '/api/user');
        $response->assertStatus(401);

        Passport::actingAs(app(User::class));
        $response = $this->json('GET', '/api/user');
        $response->assertStatus(200);
    }
    public function testCanRequestToUserStoreAndValidateRequiredFields()
    {
        Passport::actingAs(app(User::class));
        $response = $this->json('POST', '/api/user', []);
        $response->assertStatus(400);
        $response->assertJsonFragment([
            'firstname' => ["The firstname field is required."],
            'lastname' => ["The lastname field is required."],
            'username' => ["The username field is required."],
            'password' => ["The password field is required."]
        ]);
    }
}
