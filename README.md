Deployment:

Download Zip or git clone https://gitlab.com/shernel/exam-yondu.git

cd exam-yondu/backend

composer install

cat .env.example > .env (*modify database configuration*)

php artisan migrate

php artisan db:seed

php artisan passport:keys

php artisan key:generate

php artisan serve