import store from '@/store'

export default [
{
    path: '/login',
    component: require('@/components/pages/login/login.vue').default,
    name: 'Login',
    beforeEnter: (to, from, next) => {
        if(store.getters.token) {
            next({name: '/'});
        }
        else next();
    }
},
...(function () {
    let guard = [
        {
            path: '/',
            component: require('@/components/pages/admin/dashboard.vue').default,
            name: 'Dashboard',
        },
        {
            path: '/usermgmt',
            component: require('@/components/pages/admin/usersmgmt.vue').default,
            name: 'Usermgmt',
        },
        {
            path: '/usermgmt/:user_id',
            component: require('@/components/pages/admin/usersmgmt_form.vue').default,
            name: 'UsermgmtForm',
        },
    ];
    guard.forEach((v) => {
        v.beforeEnter = (to, from, next) => {
           if(!store.getters.token) {
              next({name: 'Login'});
           }
          else next();
        };
    });

    return guard;
})(),
]