import axios from 'axios'
import store from '@/store'
import router from '@/router'
import appconfig from '@/config/app.js'

class myhttp {

	constructor() {
		['get', 'post', 'put', 'patch', 'delete' ].forEach((v) => {
			this[v] = (...args) => this.request(v, args);
		})
	}

	request(method, args)
	{
		return new Promise((resolve,reject) => {
			let url = appconfig.apiBaseurl + args[0]; args.shift();
			axios.defaults.headers.common['Authorization'] = 'Bearer '+ store.getters.token;
			axios[method].apply(null, [url, ...args])
				.then( (reponse) => { resolve(reponse) })
				.catch( (...args) => {
					if(args[0].response.status == 401)
					{
						store.dispatch('removeToken');
						router.push({ name: 'Login' });
					}
					reject.apply(this,args);
				});
		});
	}

}

export default new myhttp();