const state = {
	token: localStorage.getItem('token')
}

const mutations = {
	setToken(state, token) {
		state.token = token;
	}
}

const getters = {
	token (state) {
		return state.token;
	}
}

const actions = {
	loadToken ({ commit }){
		commit('setToken', localStorage.getItem('token'));
	},
	setToken ({ commit }, token){
		localStorage.setItem('token', token);
		commit('setToken', token);
	},
	removeToken ({ commit }){
		localStorage.clear();
		commit('setToken', null);
	}
}

export default {
  state,
  getters,
  mutations,
  actions
};