import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

const stores = {
	tokenmodule: require('./modules/token').default,
}

export default new Vuex.Store(((storemodules) => {

	let k,d = {
		state: {}, mutations: {}, getters: {}, actions: {}
	}

	for (k in d){
		for (let itenIndex in storemodules)
		{
			let v=storemodules[itenIndex].hasOwnProperty(k)?storemodules[itenIndex][k]:{};

			d[k] = { ...d[k], ...v  };
		}
    }

	return d;

})(stores))