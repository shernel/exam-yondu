const path = require("path");

module.exports = {
  outputDir: path.resolve(__dirname, "../backend/public/dist"),
  baseUrl: process.env.NODE_ENV === 'production'
  ? '/dist/'
  : '/',
  indexPath: path.resolve(__dirname, "../backend/resources/views/vue/vueindex.html"),
}

